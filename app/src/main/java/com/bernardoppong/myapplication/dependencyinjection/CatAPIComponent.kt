package com.bernardoppong.myapplication.dependencyinjection

import com.bernardoppong.myapplication.model.CatAPI
import com.bernardoppong.myapplication.model.CatAPIService
import com.bernardoppong.myapplication.viewModel.CatsListViewModel
import dagger.Component

@Component(modules = [CatAPIModule::class])
interface CatAPIComponent {

    fun inject(apiService: CatAPIService)

    fun inject(viewModel: CatsListViewModel)
}