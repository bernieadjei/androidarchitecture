package com.bernardoppong.myapplication.dependencyinjection

import com.bernardoppong.myapplication.model.CatAPI
import com.bernardoppong.myapplication.model.CatAPIService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
open class CatAPIModule {

    private val BASE_URL = "http://mroppong.com"

    @Provides
    fun getCatAPI(): CatAPI {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CatAPI::class.java)
    }

    @Provides
    open fun getCatAPIService(): CatAPIService{
        return CatAPIService()
    }
}