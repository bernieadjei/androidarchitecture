package com.bernardoppong.myapplication.model

data class Cat(
    val id: String = "",
    val image: Image,
    val weight: Weight,
    val altNames: String = "",
    val countryCode: String = "",
    val name: String = ""
)


data class Image(
    val id: String = "",
    val url: String = ""
)


data class Weight(
    val metric: String = "",
    val imperial: String = ""
)


