package com.bernardoppong.myapplication.model

import io.reactivex.Single
import retrofit2.http.GET

interface CatAPI {

    @GET("cats.json")
    fun getCats(): Single<List<Cat>>
}