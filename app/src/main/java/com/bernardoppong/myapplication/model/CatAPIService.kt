package com.bernardoppong.myapplication.model

import com.bernardoppong.myapplication.dependencyinjection.DaggerCatAPIComponent
import io.reactivex.Single
import javax.inject.Inject

class CatAPIService {

    @Inject
    lateinit var api: CatAPI

    init {
        DaggerCatAPIComponent.create().inject(this)
    }

    fun getCats(): Single<List<Cat>> = api.getCats()
}