package com.bernardoppong.myapplication.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bernardoppong.myapplication.databinding.ItemCatBinding
import com.bernardoppong.myapplication.model.Cat
import com.bernardoppong.myapplication.misc.getCircularProgressDrawable
import com.bernardoppong.myapplication.misc.loadImage

class CatsListAdapter(private val catsList: ArrayList<Cat>) :
    RecyclerView.Adapter<CatsListAdapter.CatsViewHolder>() {

    fun updateCatsList( cats: List<Cat>){
        catsList.clear()
        catsList.addAll(cats)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatsViewHolder {
        val itemBinding = ItemCatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CatsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CatsViewHolder, position: Int) {
        holder.itemBinding.catTextView.text = catsList[position].name
        holder.itemBinding.catImageView.loadImage(catsList[position].image?.url, getCircularProgressDrawable(holder.itemBinding.root.context))
    }

    override fun getItemCount() = catsList.size

    class CatsViewHolder(val itemBinding: ItemCatBinding) : RecyclerView.ViewHolder(itemBinding.root)
}