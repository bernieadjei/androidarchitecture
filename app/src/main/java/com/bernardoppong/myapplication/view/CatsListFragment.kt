package com.bernardoppong.myapplication.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bernardoppong.myapplication.databinding.FragmentCatListBinding
import com.bernardoppong.myapplication.model.Cat
import com.bernardoppong.myapplication.viewModel.CatsListViewModel

class CatListFragment : Fragment() {

    private var _binding: FragmentCatListBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: CatsListViewModel
    private val catListAdapter = CatsListAdapter(arrayListOf())

    private val catsLiveDataObserver = Observer<List<Cat>> { cats ->
        cats?.let {
            binding.catsRecyclerView.visibility = View.VISIBLE
            binding.loadingErrorTextView.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
            catListAdapter.updateCatsList(it)
        }
    }

    private val hasErrorsLiveDataObserver = Observer<Boolean> { hasError ->
        binding.loadingErrorTextView.visibility = if (hasError) View.VISIBLE else View.GONE
        if (hasError) {
            binding.catsRecyclerView.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            binding.catsRecyclerView.visibility = View.GONE
            binding.loadingErrorTextView.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCatListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CatsListViewModel::class.java)
        viewModel.cats.observe(viewLifecycleOwner, catsLiveDataObserver)
        viewModel.hasErrors.observe(viewLifecycleOwner, hasErrorsLiveDataObserver)
        viewModel.loading.observe(viewLifecycleOwner, loadingLiveDataObserver)
        viewModel.reload()

        binding.catsRecyclerView.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = catListAdapter
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.reload()
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}