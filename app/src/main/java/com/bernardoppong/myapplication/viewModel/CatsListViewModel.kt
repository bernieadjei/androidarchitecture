package com.bernardoppong.myapplication.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bernardoppong.myapplication.dependencyinjection.DaggerCatAPIComponent
import com.bernardoppong.myapplication.model.Cat
import com.bernardoppong.myapplication.model.CatAPIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CatsListViewModel(application: Application) : AndroidViewModel(application) {

    constructor(application: Application, isUnitTest: Boolean = true) : this(application) {
        injected = isUnitTest
    }

    val cats by lazy { MutableLiveData<List<Cat>>() }
    val hasErrors by lazy { MutableLiveData<Boolean>() }
    val loading by lazy { MutableLiveData<Boolean>() }
    var injected = false

    @Inject
    lateinit var apiService: CatAPIService
    private val disposable = CompositeDisposable()

    private fun injectObjects() {
        if (!injected)
            DaggerCatAPIComponent.create().inject(this)
    }

    fun reload() {
        injectObjects()
        loading.value = true
        getCats()
    }

    private fun getCats() {
        disposable.add(
            apiService.getCats()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Cat>>() {
                    override fun onSuccess(catsList: List<Cat>) {
                        cats.value = catsList
                        hasErrors.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        loading.value = false
                        hasErrors.value = true
                    }

                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}