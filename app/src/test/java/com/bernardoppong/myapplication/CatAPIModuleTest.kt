package com.bernardoppong.myapplication

import com.bernardoppong.myapplication.dependencyinjection.CatAPIModule
import com.bernardoppong.myapplication.model.CatAPIService

class CatAPIModuleTest(val mockAPIService: CatAPIService): CatAPIModule() {
    override fun getCatAPIService(): CatAPIService {
        return mockAPIService
    }
}