package com.bernardoppong.myapplication

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bernardoppong.myapplication.dependencyinjection.DaggerCatAPIComponent
import com.bernardoppong.myapplication.model.Cat
import com.bernardoppong.myapplication.model.CatAPIService
import com.bernardoppong.myapplication.model.Image
import com.bernardoppong.myapplication.model.Weight
import com.bernardoppong.myapplication.viewModel.CatsListViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor

class CatsListViewModelTest {

    @get: Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var catAPIService: CatAPIService

     val application = Mockito.mock(Application::class.java)

    var catsListViewModel = CatsListViewModel(application, true)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        DaggerCatAPIComponent.builder()
            .catAPIModule(CatAPIModuleTest(catAPIService))
            .build()
            .inject(catsListViewModel)
    }

    @Before
    fun setupRxJThreadSchedulers() {
        val instantExecution = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }

        }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { instantExecution }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { instantExecution }
    }

    @Test
    fun `getCats request is successful`() {
        val image = Image("1", "")
        val weight = Weight("metric", "imperial")
        val cat = Cat("2", image, weight, "", "DE", "Bengal")

        val testSingleObservable = Single.just(listOf(cat))

        Mockito.`when`(catAPIService.getCats()).thenReturn(testSingleObservable)
        catsListViewModel.reload()

        Assert.assertEquals(1, catsListViewModel.cats.value?.size)
        Assert.assertEquals(cat.name, catsListViewModel.cats.value?.get(0)?.name)
        Assert.assertEquals(false, catsListViewModel.hasErrors.value)
        Assert.assertEquals(false, catsListViewModel.loading.value)
    }

    @Test
    fun `getCats request fails`(){
        val testSingleObservable = Single.error<List<Cat>>(Throwable())

        Mockito.`when`(catAPIService.getCats()).thenReturn(testSingleObservable)
        catsListViewModel.reload()

        Assert.assertEquals(null, catsListViewModel.cats.value?.size)
        Assert.assertEquals(true, catsListViewModel.hasErrors.value)
        Assert.assertEquals(false, catsListViewModel.loading.value)
    }
}